USE `m152` ;


-- -----------------------------------------------------

-- Table `media`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `media` ;


CREATE TABLE IF NOT EXISTS `media` (
  `idmedia` INT(11) NOT NULL AUTO_INCREMENT,
  `nomMedia` VARCHAR(45) NULL DEFAULT NULL,
  `typeMedia` VARCHAR(45) NULL DEFAULT NULL,
  `posts_IDPosts` INT(11) NOT NULL,
  PRIMARY KEY (`idmedia`))

ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------

-- Table `posts`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `posts` ;


CREATE TABLE IF NOT EXISTS `posts` (
  `IDPosts` INT(11) NOT NULL AUTO_INCREMENT,
  `commentaire` TEXT NULL DEFAULT NULL,
  `datePost` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`IDPosts`))

ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;

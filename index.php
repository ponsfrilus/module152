<?php
require_once './function.inc.php';

$posts = getPostMedia();
$tmp = parsePostMedia($posts);
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <title>HOME!</title>
</head>

<body>
  <!-- NavBar -->
  <?php include 'navbar.php'; ?>
  <div class="container">


  <!-- Welcome -->
  <blockquote class="blockquote text-center">
    <p class="mb-0">Bienvenue !!</p>
    <footer class="blockquote-footer">Someone famous in <cite title="Source Title">IDA-D3A</cite></footer>
  </blockquote>
  <!-- Image -->
  <img src="plane.png" class="rounded mx-auto d-block" alt="cube" style="width:75%">

  <div class="container">
    <?php
    foreach ($tmp as $post) {
      // debug($post);
      // debug($post["medias"]);
        ?>
  <div class="card center">
    <div class="card-header"><?= $post["datePost"] ?></div>
    <div class="card-body">
      <h5 class="card-title"><?= $post["datePost"] ?></h5>
      <p class="card-text"><?= $post["commentaire"] ?></p>
      <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
      <div class="card-columns" style="column-count: 6">
        <?php foreach ($post["medias"] as $media):
          ?>

          <!-- Button trigger modal -->
        <div class="card text-center">
          <button type="button" class="btn" data-toggle="modal" data-target="#img-<?= $media[0] ?>">
            <img class="card-img-top img-fluid" src="uploads/<?= $media[2] ?>" alt="oups">
          </button>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="img-<?= $media[0]?>" tabindex="-1" role="dialog" aria-labelledby="imgLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <a href="uploads/<?= $media[2] ?>">
                    <img class="card-img-top img-fluid" src="uploads/<?= $media[2] ?>" alt="oups">
                  </a>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
      </div>
      </div>
    </div>
  </div>
  <?php
    }
    ?>

    </div>

      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>

<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ini_set("upload_max_filesize", "200M");
ini_set("post_max_size", "210M");
ini_set("upload_tmp_dir", "/tmp");

require 'function.inc.php';

if (isset($_POST["submit"])) {
  
  EDatabase::beginTransaction();
  
  $commentaire = $_POST['comment'];
  $datePost = $_POST['datePost'];
  
  $idPost = insertPost($commentaire, $datePost);
  if ($idPost === false) {
    EDatabase::rollBack();
    return;
  }

  if (!empty(array_filter($_FILES['media']['tmp_name']))) {
    
    foreach ($_FILES['media']['tmp_name'] as $key => $val) {
      // File upload path
      $image = $_FILES['media']['name'][$key];
      $typeMedia = $_FILES['media']['type'][$key];
      // $images = $_FILES['media']['name'];
      $imageArr = explode('.', $image); //first index is file name and second index file type
      $rand = rand(1000000, 9999999);
      $newImageName = $imageArr[0] . $rand . '.' . $imageArr[1];
      $uploadPath="./uploads/".$newImageName;
      $isUploaded=move_uploaded_file($_FILES["media"]["tmp_name"][$key], $uploadPath);
      
      if ($isUploaded) {
        echo 'successfully file uploaded';
      } else {
        echo 'something went wrong';
      }
      
      if (insertImages($newImageName, $typeMedia, $idPost) == false) {
        EDatabase::rollBack();
        return;
      }
      
    }
  }
  EDatabase::commit();
        
  
  // header('Location: index.php');
}
$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;


?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>HOME</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
  <?php include 'navbar.php'; ?>
  <div class="container">
    <form name="frm_upload" id="frm_upload" action="post.php" method="post" enctype="multipart/form-data">
      <input type="hidden" name="MAX_FILE_SIZE" value="300000" />
      <div class="form-group">
        <label for="comment">commentaire</label>
        <textarea class="form-control" name="comment" id="comment" rows="3"></textarea>
      </div>
      <div class="form-group">
        <label for="media">media</label>
        <input type="file" multiple class="form-control" name="media[]" id="media">
      </div>
      <div class="form-group">
        <label for="datePost">datePost</label>
        <input class="form-control" name="datePost" type="date" value="<?php echo $today; ?>" id="datePost">
      </div>
      <input type="submit" class="btn btn-primary" name="submit">
    </form>
  </div>
</body>
</html>

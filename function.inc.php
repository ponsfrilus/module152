<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once './db/database.php';
/**
 * Fichier de démonstration de l'utilisation de la classe EDatabase
 * avec l'appel à une méthode de l'objet PDO
 * Exemple, la méthode query() de l'objet PDO
 */
 $test = 'yo rso';

function GetPost()
{
    // $sql = "SELECT commentaire, typeMedia, imageData, datePost FROM Posts";
    $sql = "SELECT idmedia, IDPosts, nomMedia, typeMedia, commentaire, datePost FROM  media inner join posts on media.posts_IDPosts = IDPosts ORDER BY IDPosts";
    $stmt = EDatabase::prepare($sql);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_CLASS);
    return $results;

}

function getPostMedia(){
  $sql= "SELECT * from posts left join media on posts.IDPosts = media.posts_IDPosts";
  $stmt = EDatabase::prepare($sql);
  $stmt->execute();
  $results = $stmt->fetchAll(PDO::FETCH_OBJ);
  //debug($results);
  return $results;
}

function parsePostMedia($ary){
  $posts = [];
  foreach ($ary as $el) {
    // Ensure that $posts['id'] is an array
    if (!isset($posts[$el->IDPosts])) {
      $posts[$el->IDPosts] = [];
    }
    // define the "id" attribute
    $posts[$el->IDPosts]['id']= $el->IDPosts;
    $posts[$el->IDPosts]['commentaire']= $el->commentaire;
    $posts[$el->IDPosts]['datePost']= $el->datePost;

    // Ensure that $posts['id']['medias'] is an array
    if (!isset($posts[$el->IDPosts]['medias'])) {
      $posts[$el->IDPosts]['medias'] = [];
    }
    //$posts[$el->IDPosts]['media'] = [];
    $posts[$el->IDPosts]['medias'][] = array($el->idmedia, $el->typeMedia, $el->nomMedia);

  }
  // print "THIS IS OUR POSTS";
  // debug($posts);
  return $posts;
}

function insertImages($image, $typeMedia, $idPost)
{
    $sql = " INSERT INTO media (nomMedia, typeMedia, posts_IDPosts) VALUES (:i, :tm, :idp)";

    $sth = EDatabase::prepare($sql);
    try {
        $sth->execute(array(
            ':i' => $image,
            ':tm' => $typeMedia,
            ':idp' => $idPost
        ));
    } catch (PDOException $e) {
        echo 'Problème de lecture de la base de données: ' . $e->getMessage();
        return false;
    }
    // Done
    return true;
}
function insertPost($commentaire, $timeStamp)
{
    $sql = " INSERT INTO posts (commentaire, datePost) VALUES (:c, :ts)";

    $sth = EDatabase::prepare($sql);
    try {
        $sth->execute(array(
            ':c' => $commentaire,
            ':ts' => $timeStamp,
        ));
    } catch (PDOException $e) {
        echo 'Problème de lecture de la base de données: ' . $e->getMessage();
        return false;
    }
    // On retourne l'id du record inséré
    return EDatabase::lastInsertId();
}

function debug($data){
  echo "<pre>";
  var_dump($data);
  echo "</pre>";
}
